package bussiness;

import java.util.ArrayList;

public class CompositeProduct extends MenuItem {
    private ArrayList<MenuItem> compositeProduct ;

    public CompositeProduct(String name, ArrayList<MenuItem> compositeProduct) {
        this.name=name;
        this.compositeProduct=compositeProduct;
    }

    @Override
    public float computePrice() {
        float price=0;
        for (MenuItem currentItem : compositeProduct) {
            price += currentItem.getPrice();
        }
        return price;
    }

}
