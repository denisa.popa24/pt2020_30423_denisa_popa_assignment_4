package bussiness;
import java.io.Serializable;
import java.util.Objects;

public class Order implements Serializable {
    public int idOrder;
    public String orderDate;
    public int tableNr;

    public Order(int idOrder, String orderDate, int tableNr){
        this.idOrder=idOrder;
        this.orderDate = orderDate ;
        this.tableNr=tableNr;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Order)) return false;
        Order order = (Order) o;
        return getIdOrder() == order.getIdOrder() &&
                getTableNr() == order.getTableNr() &&
                Objects.equals(getOrderDate(), order.getOrderDate());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getIdOrder(), getOrderDate(), getTableNr());
    }

    public int getIdOrder() {
        return idOrder;
    }

    public void setIdOrder(int idOrder) {
        this.idOrder = idOrder;
    }

    public String getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(String  orderDate) {
        this.orderDate = orderDate;
    }

    public int getTableNr() {
        return tableNr;
    }

    public void setTableNr(int tableNr) {
        this.tableNr = tableNr;
    }

    @Override
    public String toString() {
        return "Order{" +
                "idOrder=" + idOrder +
                ", orderDate=" + orderDate +
                ", tableNr=" + tableNr +
                '}';
    }
}
