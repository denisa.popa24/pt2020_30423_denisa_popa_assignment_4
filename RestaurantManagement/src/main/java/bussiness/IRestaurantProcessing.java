package bussiness;

import java.util.ArrayList;

public interface IRestaurantProcessing {
    /**
     * @pre item != null
     * list.size@pre == list.size@post -1
     */
    void createNewMenuItem(MenuItem item);

    /**
     * @param item
     * @pre item!=null
     * @post list.size = list.size@pre +1
     */
    void deleteMenuItem(MenuItem item);

    /**
     * @param item
     * @pre item!=null
     * @post item@pre.price != item@post.price
     */
    void editMenuItem(MenuItem item);

    /**
     * @param order
     * @param item
     * @pre order != null
     * @preitem != null
     */
    void createOrder(Order order, ArrayList<MenuItem> item);

    /**
     * @pre order!=null
     */
    float orderPrice(Order order);
}
