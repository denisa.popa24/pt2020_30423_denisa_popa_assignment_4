package bussiness;

import java.io.Serializable;
import java.util.*;

public class Restaurant extends Observable implements IRestaurantProcessing, Serializable {

    private ArrayList<MenuItem> menu;
    private ArrayList<MenuItem> compositeItem = new ArrayList<MenuItem>();
    private String compositeName;
    private Map<Order, Collection<MenuItem>> mapOrderMenu;
    private ArrayList<Order> orderList;

    public Restaurant() {

        menu = new ArrayList<>();
        mapOrderMenu = new HashMap<>();
        orderList = new ArrayList<>();

    }

    public ArrayList<MenuItem> getMenu() {
        return menu;
    }

    public void setMenu(ArrayList<MenuItem> myMenu) {
        this.menu = menu;
    }

    public void setCompositeItem(ArrayList<MenuItem> comp) {
        this.compositeItem = comp;
    }

    public void setCompositeName(String name) {
        this.compositeName = name;
    }

    public ArrayList<MenuItem> getCompItem() {
        return this.compositeItem;
    }

    public String getCompositeName() {
        return this.compositeName;
    }

    public Map<Order, Collection<MenuItem>> getOrderMap() {
        return this.mapOrderMenu;
    }

    public void addOrderToList(Order order) {
        orderList.add(order);
        StringBuilder sBuilder = new StringBuilder();

        sBuilder.append("NEW order created : " + "\n\n");
        sBuilder.append("Id order : ").append(order.getIdOrder()).append("\n");
        sBuilder.append("Date : ").append(order.getOrderDate()).append("\n");
        sBuilder.append("Table : ").append(order.getTableNr()).append("\n");
        sBuilder.append("Ordered Items : " + "\n");

        Map<Order, Collection<MenuItem>> myMap = getOrderMap();
        Collection<MenuItem> list = myMap.get(order);
        for (MenuItem menuItem : list) {
            sBuilder.append(menuItem.toString());
            sBuilder.append("\n");
        }

        setChanged();
        notifyObservers(sBuilder.toString());

    }

    @Override
    public void createNewMenuItem(MenuItem item) {
        assert item != null;
        int presize = menu.size();
        float prePrice=0;
        menu.add(item);
        float postPrice=item.getPrice();
        int postsize=menu.size();
        assert presize+1==postsize;
        assert postPrice==prePrice+postPrice;
    }

    @Override
    public void deleteMenuItem(MenuItem item) {
        assert item!=null;
        int presize=menu.size();
        menu.remove(item);
        int postsize=menu.size();
        assert postsize == presize - 1;

    }

    @Override
    public void editMenuItem(MenuItem item) {
        assert item!=null;
       float postPrice=item.getPrice();
       float prePrice=0;
        String nameItem=item.getName();
        Iterator<MenuItem> it = menu.iterator();
        while (it.hasNext()) {
            MenuItem currentItem =it.next();
            if (nameItem.equals(currentItem.getName())) {

                currentItem.setPrice(item.getPrice());

                prePrice = currentItem.getPrice();
            }
        }

       it = menu.iterator();
        while (it.hasNext()) {
            MenuItem currentItem = it.next();
            currentItem.setPrice(currentItem.computePrice());
        }

        assert prePrice == postPrice;
    }




    @Override
    public void createOrder(Order order, ArrayList<MenuItem> item) {

        assert order!=null;
        assert item!=null;
        Order preOrder=order;
        mapOrderMenu.put(order,item);
        assert preOrder.equals(order);

    }

    @Override
    public float orderPrice(Order order) {

        assert order != null ;

        float price=0;
        if(this.orderList.contains(order))
        {
           Collection<MenuItem> orderedItems =  mapOrderMenu.get(order);
            for (MenuItem orderedItem : orderedItems) {
                price += orderedItem.getPrice();
            }
        }
        return price;
    }
}
