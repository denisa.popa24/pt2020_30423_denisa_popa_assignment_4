package presentation;

import bussiness.CompositeProduct;
import bussiness.MenuItem;
import bussiness.Restaurant;
import data.RestaurantSerializator;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collection;

public class CompositeItemGUI extends JFrame {

    private Restaurant restaurant;

    private JLabel titleLabel;
    private JComboBox<MenuItem> menuBox;

    private ArrayList<MenuItem> compositeItem;

    private JButton addItemButton;
    private JButton finishItemButton;

    private JTextArea addedItems;
    private JLabel addedLabel;


    public CompositeItemGUI(Restaurant restaurant) {

        compositeItem = new ArrayList<MenuItem>();
        this.restaurant = restaurant;

        this.setDefaultCloseOperation(HIDE_ON_CLOSE);
        this.setBounds(10, 10, 450, 700);
        this.getContentPane().setLayout(null);

        // use a bigger font
        Font font1 = new Font("Times New Roman", Font.PLAIN, 18);
        Font font2 = new Font("Times New Roman", Font.PLAIN, 32);

        titleLabel = new JLabel("Composite Item operations");
        titleLabel.setFont(font2);
        titleLabel.setBounds(10, 20, 450, 50);
        getContentPane().add(titleLabel);

        menuBox = new JComboBox<MenuItem>();
        menuBox.setBounds(50, 70, 300, 50);
        getContentPane().add(menuBox);

        addItemButton = new JButton("Add Item");
        addItemButton.setFont(font1);
        addItemButton.setBounds(50, 550, 125, 50);
        getContentPane().add(addItemButton);
        addItemButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {

                MenuItem chosenItem = (MenuItem) menuBox.getSelectedItem();
                compositeItem.add(chosenItem);
                addedItems.append(chosenItem.toString());
                addedItems.append("\n");
            }
        });

        finishItemButton = new JButton("Finish Item");
        finishItemButton.setFont(font1);
        finishItemButton.setBounds(250, 550, 125, 50);
        getContentPane().add(finishItemButton);
        finishItemButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {

                restaurant.setCompositeItem(finishItem());
                ArrayList<MenuItem> comp = restaurant.getCompItem();
                String iname = restaurant.getCompositeName();

                CompositeProduct cp = new CompositeProduct(iname, comp);
                cp.setPrice(0);
                float finalPrice = cp.computePrice();
                cp.setPrice(finalPrice);

                restaurant.createNewMenuItem(cp);
                RestaurantSerializator.serialize(restaurant);
                comp.clear();
                addedItems.setText("");
                JOptionPane.showMessageDialog(null, "Composite item created successfully!");
            }
        });

        addedLabel = new JLabel("Added Items");
        addedLabel.setBounds(100, 150, 100, 50);
        getContentPane().add(addedLabel);

        addedItems = new JTextArea();
        addedItems.setBounds(100, 200, 250, 300);
        addedItems.setEditable(false);
        getContentPane().add(addedItems);


    }

    public void fillBox() {
        Collection<MenuItem> list = restaurant.getMenu();

        for (MenuItem curentItem : list) {
            menuBox.addItem(curentItem);

            System.out.println(curentItem.getName());
        }
    }

    public ArrayList<MenuItem> finishItem() {
        return compositeItem;
    }
}
