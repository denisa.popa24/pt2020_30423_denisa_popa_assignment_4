package presentation;

import bussiness.Restaurant;

import javax.swing.*;
import java.awt.*;
import java.util.Observable;
import java.util.Observer;

public class ChefGUI extends JFrame implements Observer {

    private JLabel titleGuiLabel;
    private Restaurant restaurant;

    public ChefGUI(Restaurant restaurant) {
        this.restaurant = restaurant;
        restaurant.addObserver(this);
        this.setDefaultCloseOperation(HIDE_ON_CLOSE);
        this.setBounds(200, 200, 150, 150);
        this.getContentPane().setLayout(null);
        Font font1 = new Font("Long Island", Font.PLAIN, 12);

        titleGuiLabel = new JLabel("Chef operations !");
        titleGuiLabel.setFont(font1);
        titleGuiLabel.setBounds(10, 50, 140, 50);
        getContentPane().add(titleGuiLabel);

    }

    @Override
    public void update(Observable o, Object arg) {
        this.setVisible(true);
        System.out.println(arg.toString());
        int x = JOptionPane.showConfirmDialog(null, arg, "Notification !", 2);
        if (x == 0) {
            System.out.println("Chef will cook!");
            this.setVisible(false);
        } else {
            System.out.println("Chef now cook, the order must wait!");
            this.setVisible(false);
        }
    }
}
