package presentation;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import java.io.FileOutputStream;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import javax.swing.table.DefaultTableModel;

import bussiness.*;
import bussiness.MenuItem;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;
import data.RestaurantSerializator;

public class WaiterGUI extends JFrame implements IRestaurantProcessing {

    private JLabel titleLabel;

    private JButton backButton;
    private JButton createOrderButton;
    private JButton addMenuItemButton;
    private JButton fillMenuButton;

    private JButton showOrdersButton;
    private JButton generateBillButton;

    ArrayList<MenuItem> orderedItems = new ArrayList<MenuItem>();
    ArrayList<Order> orderList = new ArrayList<Order>();

    private JLabel orderIDLabel;
    private JTextField orderIDField;
    private JLabel orderTableLabel;
    private JTextField orderTableField;

    private JLabel chosenItemsLabel;
    private JTextArea chosenItems;

    private JComboBox<MenuItem> menu;

    private Restaurant restaurant;
    ChefGUI chefGUI;

    public WaiterGUI(Restaurant restaurant) {
        this.restaurant = restaurant;

        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setBounds(50, 50, 1500, 700);
        this.getContentPane().setLayout(null);

        // use a bigger font
        Font biggerFont = new Font("Times New Roman", Font.PLAIN, 18);
        Font hugeFont = new Font("Times New Roman", Font.PLAIN, 32);

        titleLabel = new JLabel("Waiter operations");
        titleLabel.setFont(hugeFont);
        titleLabel.setBounds(50, 50, 450, 50);
        getContentPane().add(titleLabel);

        orderIDLabel = new JLabel("OrderId:");
        orderIDLabel.setFont(biggerFont);
        orderIDLabel.setBounds(50, 110, 80, 30);
        getContentPane().add(orderIDLabel);

        orderIDField = new JTextField();
        orderIDField.setBounds(120, 110, 50, 30);
        getContentPane().add(orderIDField);

        orderTableLabel = new JLabel("TableNr:");
        orderTableLabel.setFont(biggerFont);
        orderTableLabel.setBounds(30, 150, 90, 30);
        getContentPane().add(orderTableLabel);

        orderTableField = new JTextField();
        orderTableField.setBounds(120, 150, 50, 30);
        getContentPane().add(orderTableField);

        menu = new JComboBox<MenuItem>();
        menu.setBounds(50, 250, 250, 50);
        getContentPane().add(menu);

        chosenItemsLabel = new JLabel("Chosen items");
        chosenItemsLabel.setFont(biggerFont);
        chosenItemsLabel.setBounds(310, 130, 100, 20);
        getContentPane().add(chosenItemsLabel);

        chosenItems = new JTextArea();
        chosenItems.setBounds(310, 150, 200, 200);
        getContentPane().add(chosenItems);

        fillMenuButton = new JButton("Show menu");
        fillMenuButton.setBounds(20, 320, 110, 30);
        getContentPane().add(fillMenuButton);
        fillMenuButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                fillMenu();
                RestaurantSerializator.serialize(restaurant);
            }
        });

        addMenuItemButton = new JButton("Add");
        addMenuItemButton.setBounds(140, 320, 80, 30);
        getContentPane().add(addMenuItemButton);
        addMenuItemButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                setVisible(true);
                MenuItem myItem = (MenuItem) menu.getSelectedItem();
                assert myItem != null;
                chosenItems.append(myItem.toString());
                chosenItems.append("\n");

                orderedItems = new ArrayList<>();

                orderedItems.add(myItem);
                System.out.println(myItem.getName());
                RestaurantSerializator.serialize(restaurant);
            }

        });

        createOrderButton = new JButton("Create Order");
        createOrderButton.setFont(biggerFont);
        createOrderButton.setBounds(50, 380, 150, 50);
        getContentPane().add(createOrderButton);
        createOrderButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                ArrayList<MenuItem> copyList = null;

                try {
                    int ordID = OrderIdGenerator.generateId();
                    int table = Integer.parseInt(orderTableField.getText());
                    DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy MM dd HH.mm.ss");
                    LocalDateTime now = LocalDateTime.now();
                    String date = dtf.format(now);

                    Order myOrder = new Order(ordID, date, table);
                    myOrder.setIdOrder(ordID);
                    myOrder.setOrderDate(date);
                    myOrder.setTableNr(table);
                    System.out.println(ordID + " " + table + " " + date);
                    copyList = new ArrayList<MenuItem>();
                    Iterator<MenuItem> it = orderedItems.iterator();

                    int i = 0;
                    while (it.hasNext()) {
                        i++;
                        copyList.add(it.next());
                    }

                    if (i > 0) {
                        createOrder(myOrder, copyList);
                        chosenItems.setText("");
                        orderList.add(myOrder);
                        restaurant.addOrderToList(myOrder);
                        chefGUI.update(restaurant, myOrder);
                    } else
                        JOptionPane.showMessageDialog(null, "Nothing to deliver to chef !");

                } catch (Exception e1) {
                    System.out.println(e1);
                    JOptionPane.showMessageDialog(null, "Bad input!");
                }
                RestaurantSerializator.serialize(restaurant);
            }
        });


        showOrdersButton = new JButton("Show Orders");
        showOrdersButton.setFont(biggerFont);
        showOrdersButton.setBounds(50, 550, 150, 50);
        getContentPane().add(showOrdersButton);
        showOrdersButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                Map<Order, Collection<MenuItem>> map = restaurant.getOrderMap();
                setVisible(true);
                String columns[] = {"OrderID", "Date", "Table", "OrderedItems"};

                DefaultTableModel myModel = new DefaultTableModel();
                myModel.setColumnIdentifiers(columns);

                Object[] obj = new Object[4];
                for (Order curentOrder : orderList) {
                    obj[0] = curentOrder.getIdOrder();
                    String date = curentOrder.getOrderDate();
                    obj[1] = date;
                    obj[2] = curentOrder.getTableNr();
                    Collection<MenuItem> listM = map.get(curentOrder);
                    StringBuilder sb = new StringBuilder();
                    Iterator<MenuItem> it = listM.iterator();
                    while (it.hasNext()) {

                        sb.append(it.next().toString());
                        if (it.hasNext())
                            sb.append(" , ");
                    }

                    obj[3] = sb.toString();
                    myModel.addRow(obj);
                }

                JTable myTable = new JTable(myModel);
                JScrollPane myScrollPane = new JScrollPane();
                myScrollPane.setBounds(500, 100, 800, 400);
                myScrollPane.setViewportView(myTable);
                myScrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
                myScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
                getContentPane().add(myScrollPane);
                RestaurantSerializator.serialize(restaurant);
            }
        });


        generateBillButton = new JButton("Generate Bill");
        generateBillButton.setBounds(50, 450, 150, 50);
        getContentPane().add(generateBillButton);
        generateBillButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                int ordID = Integer.parseInt(orderIDField.getText());
                Order myOrder = findOrderByID(ordID);
                if (myOrder == null)
                    JOptionPane.showMessageDialog(null, "Order not found!");
                else {

                    StringBuilder sBuilder = new StringBuilder();
                    setVisible(true);
                    Map<Order, Collection<MenuItem>> map = restaurant.getOrderMap();
                    Collection<MenuItem> list = map.get(myOrder);
                    for (MenuItem menuItem : list) {
                        sBuilder.append(menuItem.toString());
                        sBuilder.append("\n");
                    }
                    sBuilder.append("Total price : ").append(orderPrice(myOrder));

                    try {
                        GenerateBill(myOrder, sBuilder);
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }

                    JOptionPane.showMessageDialog(null, "Bill generated successfully !");

                }
                RestaurantSerializator.serialize(restaurant);
            }
        });

    }

    @Override
    public void deleteMenuItem(MenuItem item) {
        System.out.println("Not qualified to delete menu items");

    }

    @Override
    public void editMenuItem(MenuItem item) {
        System.out.println("Not qualified to edit menu items");

    }


    @Override
    public float orderPrice(Order order) {
        return restaurant.orderPrice(order);
    }

    @Override
    public void createOrder(Order order, ArrayList<MenuItem> menuItem) {

        restaurant.createOrder(order, menuItem);

    }


    public void fillMenu() {
        ArrayList<MenuItem> list = restaurant.getMenu();
        Iterator<MenuItem> it = list.iterator();


        while (it.hasNext()) {
            MenuItem curentItem = it.next();
            menu.addItem(curentItem);
        }
    }

    public Order findOrderByID(int id) {
        Order myOrder = null;
        Iterator<Order> it = orderList.iterator();
        while (it.hasNext()) {
            Order curOrder = it.next();
            if (curOrder.getIdOrder() == id)
                myOrder = curOrder;
        }
        return myOrder;
    }

    @Override
    public void createNewMenuItem(MenuItem item) {

    }

    public static void GenerateBill(Order order, StringBuilder sBuilder) throws Exception {
        String billName = "Bill" + order.getIdOrder() + order.getOrderDate() + ".pdf";
        String text = "Order: " + order.toString() + sBuilder;
        Document document = new Document();
        PdfWriter.getInstance(document, new FileOutputStream(billName));
        document.open();
        com.itextpdf.text.Font font = FontFactory.getFont(FontFactory.COURIER, 16, BaseColor.BLACK);
        Paragraph para = new Paragraph(text, font);
        document.add(para);
        document.close();
    }
}
