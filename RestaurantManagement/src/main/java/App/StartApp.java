package App;

import bussiness.Restaurant;
import data.Controller;
import data.RestaurantSerializator;

public class StartApp {
    public static void main(String[] args) {

        Restaurant restaurant = RestaurantSerializator.deserialize();
        Controller c = new Controller();
        c.start();
        RestaurantSerializator.serialize(restaurant);
    }
}
