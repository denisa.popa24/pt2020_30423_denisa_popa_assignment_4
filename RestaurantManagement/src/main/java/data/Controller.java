package data;

import bussiness.Restaurant;
import presentation.*;


public class Controller {
    private Restaurant restaurant = RestaurantSerializator.deserialize();
    // private UserInterface userInterface=new UserInterface();
    WaiterGUI waiterGUI = new WaiterGUI(restaurant);
    ChefGUI chefGUI = new ChefGUI(restaurant);

    CompositeItemGUI compositeItemGUI = new CompositeItemGUI(restaurant);
    AdminGUI adminGUI = new AdminGUI(restaurant, compositeItemGUI);


    public void start() {

        chefGUI.setVisible(true);
        waiterGUI.setVisible(true);
        compositeItemGUI.setVisible(true);
        adminGUI.setVisible(true);

    }
}

