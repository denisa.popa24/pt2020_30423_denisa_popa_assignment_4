package data;

import bussiness.Restaurant;

import java.io.*;

public class RestaurantSerializator implements Serializable {
    public static void serialize(Restaurant restaurant){
        try {
            FileOutputStream fileOut = new FileOutputStream("D:\\Assigment4PT2020\\pt2020_30423_denisa_popa_assignment_4\\RestaurantManagement\\Restaurant.ser");
            ObjectOutputStream out = new ObjectOutputStream(fileOut);
            out.writeObject(restaurant);
            out.close();
            fileOut.close();
        } catch (IOException i) {
            i.printStackTrace();
        }
    }

    public static Restaurant deserialize(){
        Restaurant restaurant=null;
        try {
            FileInputStream fileIn = new FileInputStream("D:\\Assigment4PT2020\\pt2020_30423_denisa_popa_assignment_4\\RestaurantManagement\\Restaurant.ser");
            ObjectInputStream in = new ObjectInputStream(fileIn);
            restaurant = (Restaurant) in.readObject();
            in.close();
            fileIn.close();
            return restaurant;
        } catch (IOException i) {
            System.out.println(i.getMessage());
        } catch (ClassNotFoundException c) {
            c.printStackTrace();

        }
    return  new Restaurant();
}

}
